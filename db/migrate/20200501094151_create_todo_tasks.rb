class CreateTodoTasks < ActiveRecord::Migration
  def change
    create_table :todo_tasks do |t|
      t.string :name
      t.integer :status
      t.belongs_to :user
      t.string :detail
      
      t.timestamps null: false
    end
  end
end
