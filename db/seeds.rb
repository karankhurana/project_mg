# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


admin = Role.create(name: 'admin')
developer = Role.create(name: 'developer')

a1 = User.create(name: 'a1', email: 'a1@l.com', role: admin)
d1 = User.create(name: 'd1', email: 'd1@l.com', role: developer)
d2 = User.create(name: 'd2', email: 'd2@l.com', role: developer)