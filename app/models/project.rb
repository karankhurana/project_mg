class Project < ActiveRecord::Base
  has_many :project_users, inverse_of: :project
  has_many :users, through: :project_users, inverse_of: :projects

  belongs_to :manager, class_name: "User", foreign_key: "manager_id"
  accepts_nested_attributes_for :project_users
  accepts_nested_attributes_for :users

  validates_presence_of :name
  validates_uniqueness_of :name

  scope :active, -> { includes(:project_users).where( project_users: { active: true } ) } 
end
