class TodoTask < ActiveRecord::Base
  belongs_to :user
  validates :name, uniqueness: true, presence: true

  enum status: { created: 0, in_progres: 4, completed: 8 }
  validates :status, presence: true
end
