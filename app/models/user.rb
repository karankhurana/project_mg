class User < ActiveRecord::Base
  has_many :project_users, inverse_of: :user, dependent: :destroy
  has_many :projects, through: :project_users, inverse_of: :users, dependent: :destroy
  belongs_to :role
  has_many :todo_tasks, dependent: :destroy
  accepts_nested_attributes_for :projects
  accepts_nested_attributes_for :project_users

  validates :email, uniqueness: true, presence: true

end
