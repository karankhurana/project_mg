class Api::UsersController < Api::BaseController
  before_action :authorize_admin_action, only: [:assign_todo_task]
  
  def create
  end

  def index
    @users = User.all
    render json: @users
  end

  def assign_todo_task
    todo = TodoTask.where(name: params[:name]).last
    user    = User.where(id: params[:id]).last
    if (user && todo)
      user.todo_tasks << todo
      render json: { sucess: true, message: "TodoTask #{todo.name} is assigned to user #{params[:email]}" }
    else
      render json: { sucess: false, message: "Please enter valid task name." }
    end
  end

  private def authorize_admin_action
    unless @user.role.name.downcase == 'admin'
      render json: { status: 403, message: "Not authorized" } and return
    end
  end
end
