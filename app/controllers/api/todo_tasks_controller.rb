class Api::TodoTasksController < Api::BaseController
  before_action :authorize_admin_action, only: [:create]

  def index
    todo_tasks = @user.todo_tasks
    render json: { sucess:true, tasks: todo_tasks }
  end

  def create
    if (params[:name] && params[:detail]).present?
      @todo_task = @user.todo_tasks.new(todo_tasks_params)
      if @todo_task.save
        render json: { success: true, todo_task: @todo_task }
      else
        render json: { success: false, message: "Project: #{@todo_task.name} creation failed. Errors: #{@todo_task.errors.full_messages.join(',')}" }
      end
    else
      render json: {success: false, message: "Enter valid name and detail params."}
    end
  end

  def update
    todo_task = @user.todo_tasks.where(id: params[:id]).last
    state = TodoTask.statuses[params[:status]]
    if (todo_task && state).present?
      todo_task.status = state
      if todo_task.save
        render json: { success: true, message: "Status changed to #{todo_task.status}!" }
      else
        render json: { success: false, message: "TodoTask: updation failed. Errors: #{todo_task.errors.full_messages.join(',')}" }
      end
    else
      render json: { success: false, message: "Enter a valid status or valid task id." }
    end
  end

  private 

  def todo_tasks_params
    {
      status: 0,
      name: params[:name],
      detail: params[:detail]
    }
  end

  def authorize_admin_action
    unless @user.role.name.downcase == 'admin'
      render json: { status: 403, message: "Not authorized" } and return
    end
  end
end
