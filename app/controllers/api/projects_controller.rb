class Api::ProjectsController < Api::BaseController
  before_action :authorize_admin_action, only: [:create, :assign]

  def create
    manager = User.find_by(email: params[:manager_email])
    if (params[:name] && manager).present?
      params[:manager_id] = manager.id
      @project = Project.new(project_params)
      if @project.save!
        @project.project_users.create(user_id: @user.id, active: true)
        render json: { success: true, message: " Project #{@project.name} created successfully" }
      else
        render json: { success: false, message: "Project: #{@project.name} creation failed. Errors: #{@project.errors.full_messages.join(',')}" }
      end
    else
      render json: { sucess: false, message: "Enter project name and manager email" }
    end
  end

  def index
    @projects = Project.all.active
    render json: @projects
  end

  def assign
    project = Project.where(id: params[:id]).last
    user    = User.where(email: params[:email]).last
    if (user && project)
      project.users << user
      render json: { sucess: true, message: "Project #{project.name} is assigned to user #{params[:email]}" }
    else
      render json: { sucess: false, message: "Please enter valid email and project id" }
    end
  end

  private

    def project_params
      params.permit(:name, :manager_id)
    end

    def authorize_admin_action
      unless @user.role.name.downcase == 'admin'
        render json: { status: 403, message: "Not authorized" } and return
      end
    end
end
