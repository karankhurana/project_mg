class Api::BaseController < ApplicationController
  respond_to :json
  before_action :authenticate_user

  private

  def authenticate_user
    @user = User.where(email: request.headers['email']).last
    unless @user.present?
      render json: { status: 401, message: "Not Authorized" } and return
    end
  end
end
